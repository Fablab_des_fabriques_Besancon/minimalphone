#include <SoftwareSerial.h>

SoftwareSerial SIM900(7, 8);                  // Liaison module GSM via pins 7 et 8 de l’Arduino
const int boutonStop = 10; // Pin du bouton pour raccrocher
const int bouton1 = 11; // Pin du bouton pour appeler personne 1
const int bouton2 = 12; // Pin du bouton pour appeler personne 2
const int bouton3 = 13; // Pin du bouton pour appeler personne 3
const int pinSIM900 = 9; // Pin pour allumer le module SIM900

int etatboutonStop = HIGH;
int etatbouton1 = HIGH;
int etatbouton2 = HIGH;
int etatbouton3 = HIGH;

// Paramétrage du debounce pour les boutons
long lastDebounceTime = 0; 
long debounceDelay = 200;

char network_name[10] = " "; // on réserve une chêne de caractère pour le réseau
char c; // pour récupérer les réponses du module SIM900

////////////////////////////////////////////////////////////////////////////////////////////////////
// Intialisation

void setup() {

  Serial.begin(115200); //Notre liaison série
  pinMode(pinSIM900, OUTPUT);
  digitalWrite(pinSIM900, LOW);
  delay(1000);
  pinMode(boutonStop, INPUT_PULLUP);
  pinMode(bouton1, INPUT_PULLUP);
  pinMode(bouton2, INPUT_PULLUP);
  pinMode(bouton3, INPUT_PULLUP);

  simInit();
  checkOperator();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonctions d’appel

void call1() {
  SIM900.println("ATD 0033xxxxxxxxx;");  // Appelle la personne 1 via le numéro au format international
  delay(100);
  SIM900.println();
  delay(30000); //On laisse un délai de 30s pour assurer quelques sonneries minimale sinon ça raccroche
}

void call2() {
  SIM900.println("ATD 0033yyyyyyyyyy;");  // Appelle la personne 2 via le numéro au format international
  delay(100);
  SIM900.println();
  delay(100);
  SIM900.println();
  delay(30000); //On laisse un délai de 30s pour assurer quelques sonneries minimale sinon ça raccroche
}

void call3() {
  SIM900.println("ATD 0033zzzzzzzzz");          // Appelle la personne 3 via le numéro au format international
  delay(100);
  SIM900.println();
  delay(30000); //On laisse un délai de 30s pour assurer quelques sonneries minimale sinon ça raccroche
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction de raccrochage d’appel

void stopcall(){
  SIM900.println("ATH"); // Fin de l'appel
  Serial.println("Fin de l'appel");
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction d’initialisation du shield

void simInit() {
  digitalWrite(pinSIM900, HIGH); // Allume le module
  Serial.println("On allume le module");
  delay(5000);  // Attente 5 secondes que le module s'allume
  SIM900.begin(19200); // Arduino communique avec SIM900 GSM aune vitesse de 19200
  // Réglages du module
  SIM900.println("AT+CLVL=100"); // réglage du volume du haut parleur 0-100
  simReply();
  Serial.println("Volume à 100");
  SIM900.println("AT+CMIC=0,15"); // réglage du gain micro 0-15
  simReply();
  delay(20000); // Attente 20 secondes pour rejoindre le réseau
}

//////////////////////////////////////////////////////////////////////////////////////////
// Fonction de récupération des réponses aux commandes envoyées au shield

void simReply() { 
  delay(500);
  while (SIM900.available()) {
    char c = SIM900.read();
    if (c != '\n') Serial.write(c);
    else Serial.print(" ");
    delay(5);
  }
  Serial.println();
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Fonction de récupération du nom de l’opérateur réseau GSM
void checkOperator() {
  SIM900.println("AT+COPS?"); //Opérateur
  delay(100);
  if (SIM900.find("\"")) { //Extraction du nom de l’opérateur
    c = SIM900.read();
    int u = 0;
    while (c != '"' && u < 10) {
      network_name[u] = c;
      c = SIM900.read();
      u++;
    }
  }
  Serial.println("Réseau trouvé : ");
  Serial.print(network_name);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// Boucle principale

void loop() {
  etatbouton1 = digitalRead(bouton1);
  etatbouton2 = digitalRead(bouton2);
  etatbouton3 = digitalRead(bouton3);
  etatboutonStop = digitalRead(boutonStop);

  //filtrage debounce
  if ( (millis() - lastDebounceTime) > debounceDelay) {

    if ((etatbouton1 == LOW)) {
      Serial.println("Appel personne 1 en cours");
      call1();
      lastDebounceTime = millis(); //set the current time
    }
    if ((etatboutonPhi == LOW)) {
      Serial.println("Appel personne 2 en cours");
      call2();
      lastDebounceTime = millis(); //set the current time
    }
    if ((etatboutonSyl == LOW)) {
      Serial.println("Appel personne 3 en cours");
      call3();
      lastDebounceTime = millis(); //set the current time
    }
    if ((etatboutonStop == LOW)) {
      Serial.println("On raccroche");
      stopcall();
      lastDebounceTime = millis(); //set the current time
    }
    delay(100);
  }
}